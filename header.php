<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ultherapy
 */
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="keywords" content="Ultherapy, Cosmetic, Clinic, Medical, Laser, Treatment, Skin, Tightening, Specialists">
        <meta name="description" content="At the Cosmetic Medical Clinic we are proud to have been the first company in Australia to offer the Ultherapy Procedure to the public in 2008 and to be recognised as the pioneers in developing and implementing new and effective methods of application for this advanced procedure.">
        <meta name="author" content="cmcnsw">
        <?php wp_head(); ?>
</head>

<body>
    <div id="preload">
        <div id="load"></div>
    </div>

    <?php get_template_part('partials/navigation-top-bar'); ?>