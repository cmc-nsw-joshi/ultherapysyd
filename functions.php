<?php
/**
 * Theme functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @author SJ
 * @package Ultherapy
 */

// Includes
include(get_template_directory() . '/includes/front/enqueue.php');
include(get_template_directory() . '/includes/setup.php');
include(get_template_directory() . '/includes/customizer/miscellaneous.php');
include(get_template_directory() . '/includes/theme-customizer.php');
include(get_template_directory() . '/includes/customizer/social.php');
//include(get_template_directory() . '/includes/widgets.php');

// Hooks
add_action('wp_enqueue_scripts', 'cmc_ultherapy_enqueue');
add_action('after_setup_theme', 'cmc_ultherapy_setup_theme');
add_action('customize_register', 'cmc_ultherapy_customize_register');
//add_action('widgets_init', 'cmc_ultherapy_widgets');

// Filters


// Shortcodes

