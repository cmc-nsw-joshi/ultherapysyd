<?php wp_footer(); ?>

<section class="pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title">
                    <h2 class="wow fadeInUp" data-wow-duration=".3s" data-wow-delay=".3s">Subscribe to Newsletter</h2>
                    <p>Lorem ipsum, or lipsum as it is sometimes known is a<br>  text usedin laying out print, graphic </p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form class="subscribe-form-section" action="#">
                    <input type="text" placeholder="Enter your email here">
                    <button class="btn subs-btn">Subscribe</button>
                </form>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="social-div">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer-section">
   <div class="container">
        <div class="
        row">
            <div class="col-lg-12">
                <p>© Copyright 2018. All Rights Reserved. Designed by <a href="#">Synchrotheme</a></p>
            </div>
        </div>
    </div>
</footer>