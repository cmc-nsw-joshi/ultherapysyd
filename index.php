<?php get_header(); ?>

<!--Home Section Start-->
<section id="home" class="banner banner-default banner-wave" data-stellar-background-ratio=".7" data-scroll-index="0" 
    style="background-image: url(<?php echo get_theme_mod('cmc_ultherapy_setting_homepage_image'); ?>); 
            background-size: cover; background-repeat: no-repeat;
            background-attachment: fixed; position: relative;">

    <?php if(get_theme_mod('cmc_ultherapy_setting_show_overlay')) : ?> 
    <div class="overlay">
    <?php endif; ?>        
    
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-8 d-flex h-100 align-items-center">
                    <div class="header-app">
                        <h2 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">Discover Ultherapy </br> Easy & Pain-free</h2>

                        <p> The onyl non-invasive procedure available to actually fit skin on the face, neck and chest.
                        We offer consultations at no cost and are happy to organise a time to sit down and discuss your options and whether or not Ultherapy is the right procedure for you.
                        </p>
                        <div >
                        </div>
                        <!--
                        <div class="header-btn">
                            <a class="header-btn-1 header-btn-color"><i class="fab fa-google-play"></i> Google Play</a>
                            <a class="header-btn-1"><i class="fab fa-apple"></i> Apple Store</a>
                        </div>
                        -->
                        <div class="header-btn">
                        <a class="header-btn-1 header-btn-color"><i class="fab fa-google-play"></i> FIND OUT MORE</a>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-lg-4 d-flex justify-content-center">
                    <div class="header-slides owl-carousel mt-150 text-center">
                        <div class="single-mobile">
                            <img class="img-fluid" src="assets/images/scrn/dummy-image.png" alt="">
                        </div>
                        <div class="single-mobile">
                            <img class="img-fluid" src="assets/images/scrn/dummy-image.png" alt="">
                        </div>
                        <div class="single-mobile">
                            <img class="img-fluid" src="assets/images/scrn/dummy-image.png" alt="">
                        </div>
                        <div class="single-mobile">
                            <img class="img-fluid" src="assets/images/scrn/dummy-image.png" alt="">
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
    <?php if(get_theme_mod('cmc_ultherapy_setting_show_overlay')) : ?> 
    </div>
    <?php endif; ?>  
</section>
<section class="app-features pt-100 pb-100" >
    <div class="container">
        <div class="row">
            <div class="col-lg-4">

                <div class="blog-post-wrapper-div">
                    <div>
                        <div class="post-detail-container-div">
                            <div class="post-content-div">
                            <div class=" features-item-1">
                                <a href="#"><i class="fa fa-info"></i></a>
                            </div>
                                <h3 class="post-title entry-title">
                                <a href="blog-details-right-sidebar.html">About Us</a>
                                </h3>

                                <p class="post-excerpt">Why are we recognised as the leading provider of Ultherapy in Australia?</p>
                                <div class="view_detail">
                                <a href="blog-details-right-sidebar.html" class="blog-btn">FIND OUT <i class="fas fa-arrow-right"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class=" features-item-1">
                    <a href="#"><i class="fa fa-comments"></i></a>
                    <h4>FAQ</h4>
                    <p>Read about the most commonly asked questions for Ultherapy.</p>
                    <h3>Get the FAQ's</h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class=" features-item-1">
                    <i class="fa fa-user-md"></i>
                    <h4>Contact Us</h4>
                    <p>Get in touch with us to find out if Ultherapy is right for you.</p>
                    <h3>Contact Us</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pt-100 pb-100 bg-gray">
          <div class="container">
              <div class="row">
                 <div class="col-lg-6 d-flex align-items-center">
                     <div class="about-app-sec-text about-app-left">
                         <div class="about-app-item">
                            <i class="flaticon-021-mobile-app"></i>
                         </div>
                         <div class="about-app-text">
                             <h2>Beautiful , Modern & unique energetic Design </h2>
                             <p>In a professional context it often happens that private or corporate clients corder a publication to be made and presented with the actual content still not being ready. Think of a news blog that's filled with content hourly on the day of going live.</p>
                             <ul class="about-app-text-item">
                               <li><i class=" fas fa-check"></i> Lorem Ipsum has been the industry's standard.</li>
                               <li><i class="fas fa-check"></i>The professional context it often happens that.</li>
                               <li><i class="fas fa-check"></i> corporate clients corder a publication made.</li>
                             </ul>
                         </div>
                         <div class="about-app-section-btn">
                            <a class="about-more-btn" href="#">Learn More</a>
                            <a class="about-more-btn" href="#">Download </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-6 d-flex align-items-center justify-content-center">
                     <div class="about-app-sec-img">
                         <img src="assets/images/dummy-image-3.png" alt="">
                     </div>
                 </div>
              </div>
          </div>
        </section>
<section class="section-bg counter-area">
    <?php if(get_theme_mod('cmc_ultherapy_setting_show_overlay')) : ?> 
    <div class="overlay pt-100 pb-100">
    <?php else: ?>
    <div class="pt-100 pb-100">  
    <?php endif; ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-title title-bg">
                        <h2 class="wow fadeInUp" data-wow-duration=".3s" data-wow-delay=".3s">Download Your App </h2>
                        <p>Lorem ipsum, or lipsum as it is sometimes known is a<br>  text usedin laying out print, graphic </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <!--Stats Item-->
                            <div class="counter-item counter-item-1">
                                <div class=" counter-text counter-bg-white">
                                <h6>Total Download</h6>
                                    <p><span class="counter" data-count="1100">1,033</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <!--Stats Item-->
                            <div class="counter-item counter-item-1">
                                    <div class=" counter-text counter-bg-white">
                                    <h6>Happy Clients</h6>
                                        <p><span class="counter" data-count="699">1,033</span></p>
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <!--Stats Item-->
                            <div class="counter-item  counter-item-1">
                                <div class="counter-text counter-bg-white">
                                    <h6>Active Install</h6>
                                    <p><span class="counter" data-count="789">1,033</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a class="header-btn-1 header-btn-color"><i class="fab fa-google-play"></i> Google Play</a>
                            <a class="header-btn-1"><i class="fab fa-apple"></i> Apple Store</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>