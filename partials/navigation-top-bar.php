<nav class="navbar navbar-expand-lg">
    <div class="container">
    <a class="navbar-brand" href="<?php echo home_url('/');?>"> 
        <?php if(has_custom_logo()) : 
            the_custom_logo(); ?>
            <a href="<?php echo home_url('/');?>" class="custom-logo-link" rel="home" itemprop="url">
                <img width="170" height="50" class="color-logo" src="<?php echo get_theme_mod('my_site_logo_id'); ?>" alt="logo">
            </a>
        <?php else: ?>
            <a href="<?php echo home_url('/');?>" class="color-logo"><?php bloginfo( 'name' ); ?></a>
        <?php endif; ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-bars"></span>
    </button>
    <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav ml-auto">  
            <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="0">Home</a>
            </li>
        <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="1">Features</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="2">team</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="3">Pricing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="4">Faqs</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Blog
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item " href="blog-grid-left-sidebar.html">Grid Left Sidebar</a>
                <a class="dropdown-item " href="blog-grid-right-sidebar.html">Grid Right Sidebar</a>
                <a class="dropdown-item " href="blog-grid-no-sidebar.html">Grid No Sidebar</a>
                <a class="dropdown-item " href="blog-details-left-sidebar.html"> Single Left Sidebar</a>
                <a class="dropdown-item " href="blog-details-right-sidebar.html">Single Right Sidebar</a>
                <a class="dropdown-item " href="blog-details-no-sidebar.html"> Single No Sidebar</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Pages
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                <a class="dropdown-item" href="features-page.html">Features</a>
                <a class="dropdown-item" href="about-us.html"> About</a>
                <a class="dropdown-item " href="services.html"> Services</a>
                <a class="dropdown-item " href="404-page.html">404 page</a>
                <a class="dropdown-item " href="contact.html"> Contact</a>
                <a class="dropdown-item " href="login.html"> Log In</a>
                <a class="dropdown-item " href="signin.html"> Register</a>
                <a class="dropdown-item " href="forget-password.html"> Lost Password</a>
            </div>
        </li>
        </ul>
    </div>
    </div>
</nav>
<!--Navbar End-->