 <?php
 /**
 * Enqueue scripts and styles.
 */

 function cmc_ultherapy_enqueue() {
    $min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

    // Styles
    wp_enqueue_style( 'cmc_ultherapy_css_google_fonts_exo', 'https://fonts.googleapis.com/css?family=Exo:400,600,700' );

    wp_enqueue_style( 'cmc_ultherapy_css_font_awesome', get_template_directory_uri() . '/assets/css/fontawesome-all.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_flat_icon', get_template_directory_uri() . '/assets/css/flaticon.css' );

    wp_enqueue_style( 'cmc_ultherapy_css_bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.css' );

    wp_enqueue_style( 'cmc_ultherapy_css_owl_carousel', get_template_directory_uri() . '/assets/css/owl.carousel' . $min .'.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_owl_theme', get_template_directory_uri() . '/assets/css/owl.theme.default' . $min .'.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_jquery_bxslider', get_template_directory_uri() . '/assets/css/jquery.bxslider' . $min .'.css' );

    wp_enqueue_style( 'cmc_ultherapy_css_magnific_popup', get_template_directory_uri() . '/assets/css/magnific-popup.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_animate', get_template_directory_uri() . '/assets/css/animate' . $min .'.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_progress_circle', get_template_directory_uri() . '/assets/css/progresscircle.css' );

    wp_enqueue_style( 'cmc_ultherapy_css_google_fonts_swiper', get_template_directory_uri() . '/assets/css/swiper.css' );

    wp_enqueue_style( 'cmc_ultherapy_css_site_main_style', get_template_directory_uri() . '/assets/css/style.css' );
    wp_enqueue_style( 'cmc_ultherapy_css_site_main_page', get_template_directory_uri() . '/assets/css/page.css' );

    // Scripts
    wp_enqueue_script('cmc_ultherapy_js_jquery', get_template_directory_uri() . '/assets/js/jquery.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap' . $min .'.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_stellar', get_template_directory_uri() . '/assets/js/jquery.stellar.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_animated_headline', get_template_directory_uri() . '/assets/js/animated.headline.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_owl_carousel', get_template_directory_uri() . '/assets/js/owl.carousel' . $min .'.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_scrollIt', get_template_directory_uri() . '/assets/js/scrollIt' . $min .'.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd' . $min .'.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_magnific_popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup' . $min .'.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_swiper', get_template_directory_uri() . '/assets/js/swiper' . $min .'.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_jquery_bxslider', get_template_directory_uri() . '/assets/js/jquery.bxslider' . $min .'.js', array(), false, true );

    wp_enqueue_script('cmc_ultherapy_js_site_main_contact', get_template_directory_uri() . '/assets/js/contact.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_site_main_wow', get_template_directory_uri() . '/assets/js/wow' . $min .'.js', array(), false, true );
    wp_enqueue_script('cmc_ultherapy_js_site_main', get_template_directory_uri() . '/assets/js/main.js', array(), false, true );
 }