<?php
function cmc_ultherapy_miscellaneous_customizer_section( $wp_customize ){
    /**
     * Global section
     */
    $wp_customize->add_section( 'cmc_ultherapy_section_global', array(
		'title'         =>  __( 'Global Settings', 'cmc_ultherapy' ),
		'priority'      =>  30
    ));

    /**
     * Add Second Logo
     */
    $wp_customize->add_setting( 'my_site_logo_id', array(
        'default'          =>  ''
	));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
            'logo',
            array(
               'label'      => __( 'Upload Second logo', 'cmc_ultherapy' ),
               'section'    => 'cmc_ultherapy_section_global',
               'settings'   => 'my_site_logo_id' 
            )
        )
    );

    /**
     * Show overlay
     */
    $wp_customize->add_setting( 'cmc_ultherapy_setting_show_overlay', array(
        'default'          =>  0
	));
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
            'cmc_ultherapy_control_show_overlay',
            array(
               'label'      => __( 'Show Overlays', 'cmc_ultherapy' ),
               'section'    => 'cmc_ultherapy_section_global',
               'settings'   => 'cmc_ultherapy_setting_show_overlay', 
               'type'       => 'checkbox'
            )
        )
    );

    /**
     * Home page section 
     */
    $wp_customize->add_section( 'cmc_ultherapy_section_home', array(
		'title'         =>  __( 'Home Settings', 'cmc_ultherapy' ),
		'priority'      =>  30
    ));

     /**
     * Add Home Page Image 1200x600
     */
    $wp_customize->add_setting( 'cmc_ultherapy_setting_homepage_image', array(
        'default'          =>  ''
	));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,
            'image',
            array(
               'label'      => __( 'Upload Homepage Image', 'cmc_ultherapy' ),
               'section'    => 'cmc_ultherapy_section_home',
               'settings'   => 'cmc_ultherapy_setting_homepage_image' 
            )
        )
    );
}
    

