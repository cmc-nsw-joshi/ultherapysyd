<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

    function cmc_ultherapy_setup_theme() {
        // Make theme available for translation.
        load_theme_textdomain( 'cmc_ultherapy' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        // Let WordPress manage the document title.
        add_theme_support( 'title-tag' );

        // Enable support for Post Thumbnails.
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'cmc-ultherapy-thumb', 360, 270 );

        // Enable support for custom logo.
        add_theme_support('custom-logo', array(
            'width'  => 170,
            'height' => 50,
        ));

        // Register nav menus.
        register_nav_menus( array(
            'primary' => esc_html__( 'Primary Menu', 'cmc_ultherapy' ),
            'footer'  => esc_html__( 'Footer Menu', 'cmc_ultherapy' ),
            'social'  => esc_html__( 'Social Menu', 'cmc_ultherapy' ),
        ) );

        // Add support for HTML5 markup.
        add_theme_support( 'html5', array(
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
    }